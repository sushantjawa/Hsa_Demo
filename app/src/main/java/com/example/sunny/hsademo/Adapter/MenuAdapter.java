package com.example.sunny.hsademo.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sunny.hsademo.Models.Menu;
import com.example.sunny.hsademo.MyRoom;
import com.example.sunny.hsademo.R;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Sunny on 14/09/2017.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder>  {
   private Context context;
    private List<Menu> menuList;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout baselayout;
        public TextView category;
        public ImageView menu_icon;

        public MyViewHolder(View view) {
            super(view);
            category=(TextView) view.findViewById(R.id.menu_name);
            baselayout=(LinearLayout) view.findViewById(R.id.baseLayout);
            menu_icon=(ImageView) view.findViewById(R.id.menu_icon);


        }
    }
    public MenuAdapter(Context mContext, List<Menu> menuList)
    {
        this.context=mContext;
        this.menuList=menuList;

    }
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_menu, parent, false);

        return new MyViewHolder(itemView);
    }
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Menu menu = menuList.get(position);
        System.out.println("In Adapter"+position+menu.getName());
       if(menu.getEnabled()==1)
       holder.category.setText(menu.getName());
           if (menu.getType().equalsIgnoreCase("myroom"))
           Picasso.with(context).load(R.drawable.bed).fit().into(holder.menu_icon);
       else  if (menu.getType().equalsIgnoreCase("food"))
            Picasso.with(context).load(R.drawable.if_food_572824).fit().into(holder.menu_icon);
        else    if (menu.getType().equalsIgnoreCase("leisure"))
               Picasso.with(context).load(R.drawable.if_cocktail_2138266).fit().into(holder.menu_icon);
          else  if (menu.getType().equalsIgnoreCase("transport"))
               Picasso.with(context).load(R.drawable.car).fit().into(holder.menu_icon);

        holder.baselayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(menu.getType().equalsIgnoreCase("myroom"))
                    context.startActivity(new Intent(context,MyRoom.class));
            }
        });


    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }
}
