package com.example.sunny.hsademo.Common;

/**
 * Created by Sunny on 11/06/2016.
 */

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

//@SuppressLint("ValidFragment")
public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    Button txtdate;
    DatePickerDialog d;

    public DateDialog() {

    }

  /*  public DateDialog(View view){
        txtdate=(Button) view;
    }*/

    public void DateDialog1(View view) {
        txtdate = (Button ) view;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {


// Use the current date as the default date in the dialog
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        d = new DatePickerDialog(getActivity(), this, year, month, day);
//return new DatePickerDialog()

        return d;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        //show to the selected date in the text box
        String date = day + "-" + (month + 1) + "-" + year;
        txtdate.setText(date);

    }
}



