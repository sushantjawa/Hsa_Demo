package com.example.sunny.hsademo.Models;

import java.util.List;

/**
 * Created by Sunny on 15/09/2017.
 */
public class CategoriesList {
    private List<Categories> categories = null;

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }
}
