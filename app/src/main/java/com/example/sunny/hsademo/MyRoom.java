package com.example.sunny.hsademo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.sunny.hsademo.Adapter.MenuAdapter;
import com.example.sunny.hsademo.Adapter.RoomAdapter;
import com.example.sunny.hsademo.Models.Categories;
import com.example.sunny.hsademo.Models.CategoriesList;
import com.example.sunny.hsademo.Models.MenuList;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sunny on 14/09/2017.
 */
public class MyRoom extends AppCompatActivity {
    Context context;
    RecyclerView recyclerView;
    private DatabaseReference firebaseDatabase;
    private CategoriesList categoriesList;
    private List<Categories> categories=new ArrayList<Categories>();
    ProgressDialog progressDialog;
    //List<String> RoomServiceString=new ArrayList<String>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_service);
        recyclerView=(RecyclerView) findViewById(R.id.recycler_view_room_service);
        Toolbar toolbar = (Toolbar) findViewById(R.id.view_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Room");
        firebaseDatabase = FirebaseDatabase.getInstance().getReference("Categories/categories");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
        categories.clear();
        ValueEventListener valueEventListener = new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //categories =dataSnapshot.getValue(Categories.class);
                // System.out.println("categories"+categoriesList.getCategories().get(2).getName());
                // CategoriesList categoriesList1=new CategoriesList();

                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren())
                {
                    Categories categories1=dataSnapshot1.getValue(Categories.class);
                    if(categories1.getType().equalsIgnoreCase("myroom"))
                        categories.add(categories1);
                    System.out.println("debug"+categories1.getType());
                }
                RoomAdapter roomAdapter =new RoomAdapter(getApplicationContext(),categories);
                recyclerView.setAdapter(roomAdapter);
                //   System.out.println("debugnew"+roomAdapter.count);

                progressDialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("categoriesf"+databaseError.toException());
                progressDialog.dismiss();
            }
        };
        firebaseDatabase.addValueEventListener(valueEventListener);

    }

    @Override
    protected void onStart() {
        super.onStart();


    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                //  startActivity(i);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void onBackPressed()
    {
        finish();
    }
}
