package com.example.sunny.hsademo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * Created by Sunny on 29/08/2017.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            // return new Schemes();
            // } //else return new Brands();
            return new LaundryMenWear();
        }
        return new LaundryMenWear();
    }

    @Override
    public int getCount() {
        return 1;
    }
}