package com.example.sunny.hsademo.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sunny.hsademo.R;


/**
 * Created by Sunny on 28/08/2017.
 */
public class HelperClass {
    public static void showErrorMsg(Context context, String message) {

        showToastBar(context, message);

    }
    public static void showToastBar(Context context, String message) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.view_custom_toast_layout, null);
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        int actionBarHeight = context.getResources().getDimensionPixelSize(R.dimen.snackbar_height);
        toast.setGravity(Gravity.BOTTOM, 0, actionBarHeight);
        toast.setView(layout);//setting the view of custom toast layout
        ((TextView) toast.getView().findViewById(R.id.custom_toast_message)).setText(message);
        toast.show();
    }
    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        boolean networkConnected = (networkInfo != null && networkInfo.isConnected());
        if (!networkConnected) showErrorMsg(context, "No Internet Connection");
        return networkConnected;
    }

}
