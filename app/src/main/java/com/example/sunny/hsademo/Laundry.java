package com.example.sunny.hsademo;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sunny.hsademo.Common.DateDialog;
import com.example.sunny.hsademo.Common.HelperClass;
import com.example.sunny.hsademo.Common.TimeDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;

public class Laundry extends AppCompatActivity {
    Context context;
    //private ViewPager viewPager;
    //private TabLayout tabLayout;
    //private String[] pageTitle = {"Men's Wear", "Womens's Wear", "Others"};
     Button DateE,TimeE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laundry);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DateE = (Button) findViewById(R.id.Date);
        TimeE = (Button) findViewById(R.id.Time);
        Button confirm = (Button) findViewById(R.id.ButtonConfirm);
        setdatetime();
        DateE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateDialog dialog = new DateDialog();
                dialog.DateDialog1(v);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "DatePicker");
                //Date.setText();
                // Startdate=date.getText().toString();
            }

        });
        TimeE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeDialog dialog = new TimeDialog();
                dialog.TimeDialog1(v);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                dialog.show(ft, "TimePicker");
                //Date.setText();
                // Startdate=date.getText().toString();
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.view_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Laundry");
        context = this;
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.dialog_custom_box, null);
                final AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setView(alertLayout);
                final TextView Title=(TextView) alertLayout.findViewById(R.id.titleText);
                final TextView Message=(TextView) alertLayout.findViewById(R.id.textMessage);
                final TextView yes=(TextView) alertLayout.findViewById(R.id.positiveButton);
                final TextView no=(TextView) alertLayout.findViewById(R.id.negativeButton);
                // Title.setText("Confirm");

                Title.setText("Confirm Order");
                Message.setVisibility(View.GONE);
                final AlertDialog dialog = alert.create();
                dialog.show();
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        HelperClass.showErrorMsg(getApplicationContext(),"Someone will come to pick up laundry soon.Thankyou.");
                        startActivity(new Intent(getApplicationContext(),HomePage.class));
                        finish();
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }
    public  void setdatetime()
    {
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm",Locale.getDefault());
        Date date=new Date();
        //Timer timer=new Timer();
        DateE.setText(dateFormat.format(date));
        TimeE.setText(timeFormat.format(date));

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                //  startActivity(i);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void onBackPressed()
    {
        finish();
    }
}