package com.example.sunny.hsademo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sunny.hsademo.Common.HelperClass;

import java.util.ArrayList;
import java.util.List;

public class Replenish extends AppCompatActivity {

    Button buttonorder;
     Context context;
    LinearLayout b1,b2,b3;
    List<String> Replenishorder=new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replenish);
        context=this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.view_toolbar );
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Replenish");
        buttonorder=(Button) findViewById(R.id.ButtonOrder);
        b1=(LinearLayout) findViewById(R.id.baseLayout1);
        b2=(LinearLayout) findViewById(R.id.baseLayout2);
        b3=(LinearLayout) findViewById(R.id.baseLayout3);
b1.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if(Replenishorder.contains("Water"))
        { b1.setBackgroundColor(getResources().getColor(R.color.white));
        Replenishorder.remove("Water");
        }
        else
        {Replenishorder.add("Water");
        b1.setBackgroundColor(getResources().getColor(R.color.teallight));
       }
    }
});
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Replenishorder.contains("Tea"))
                { b2.setBackgroundColor(getResources().getColor(R.color.white));
                    Replenishorder.remove("Tea");
                }
                else
                {Replenishorder.add("Tea");
                    b2.setBackgroundColor(getResources().getColor(R.color.teallight));
                }
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Replenishorder.contains("Coffee"))
                { b3.setBackgroundColor(getResources().getColor(R.color.white));
                    Replenishorder.remove("Coffee");
                }
                else
                {Replenishorder.add("Coffee");
                    b3.setBackgroundColor(getResources().getColor(R.color.teallight));
                }
            }
        });
    buttonorder.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           // System.out.println("asdf"+Replenishorder.get(1));
            LayoutInflater inflater = getLayoutInflater();
            View alertLayout = inflater.inflate(R.layout.dialog_custom_box, null);
            final AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setView(alertLayout);
            final TextView Title=(TextView) alertLayout.findViewById(R.id.titleText);
            final TextView Message=(TextView) alertLayout.findViewById(R.id.textMessage);
            final TextView yes=(TextView) alertLayout.findViewById(R.id.positiveButton);
            final TextView no=(TextView) alertLayout.findViewById(R.id.negativeButton);
       // Title.setText("Confirm");

            Title.setText("Confirm Order");
            Message.setVisibility(View.GONE);
            final AlertDialog dialog = alert.create();
            dialog.show();
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   dialog.dismiss();
                    HelperClass.showErrorMsg(getApplicationContext(),"Your order will be delivered soon.ThankYou.");
                    startActivity(new Intent(getApplicationContext(),HomePage.class));
                    finish();
                }
            });
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
    });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                //  startActivity(i);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void onBackPressed()
    {
        finish();
    }


}
