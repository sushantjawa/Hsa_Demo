package com.example.sunny.hsademo.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.LayoutInflater;
import com.example.sunny.hsademo.Laundry;
import com.example.sunny.hsademo.Models.Categories;
import com.example.sunny.hsademo.R;
import com.example.sunny.hsademo.Replenish;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Sunny on 14/09/2017.
 */
public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.MyViewHolder>  {
    private Context context;
    private List<Categories> categoriesList;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout baselayout;
        public TextView category;
        public ImageView categoryicon;
        public MyViewHolder(View view) {
            super(view);
            category=(TextView) view.findViewById(R.id.category_name);
            baselayout=(LinearLayout) view.findViewById(R.id.baseLayout);
            categoryicon=(ImageView) view.findViewById(R.id.category_icon);

        }
    }
    public RoomAdapter(Context mContext, List<Categories> categoriesList)
    {
        this.context=mContext;
        this.categoriesList=categoriesList;

    }
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_my_room, parent, false);

        return new MyViewHolder(itemView);
    }
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Categories categories = categoriesList.get(position);
        System.out.println("In Adapter"+position+categories.getName());

        holder.category.setText(categories.getName());

        if (categories.getSubtype().equalsIgnoreCase("laundry"))
            Picasso.with(context).load(R.drawable.iron).fit().into(holder.categoryicon);
        if (categories.getSubtype().equalsIgnoreCase("replenish"))
            Picasso.with(context).load(R.drawable.jar).fit().into(holder.categoryicon);
        if (categories.getSubtype().equalsIgnoreCase("roomservice"))
            Picasso.with(context).load(R.drawable.room_service).fit().into(holder.categoryicon);
        if (categories.getSubtype().equalsIgnoreCase("otherservice"))
            Picasso.with(context).load(R.drawable.construction).fit().into(holder.categoryicon);
        holder.baselayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(categories.getSubtype().equalsIgnoreCase("Replenish"))
                { context.startActivity(new Intent(context,Replenish.class));
                }
            //count.add(categories.getName());

                if(categories.getSubtype().equalsIgnoreCase("laundry"))
                {
                    context.startActivity(new Intent(context,Laundry.class));


                   /* final AlertDialog.Builder builder;

                        builder = new AlertDialog.Builder(view.getRootView().getContext());

                    builder.setTitle("Order Laundry")
                            .setMessage("Order Laundry to Room Number 241?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                dialog.dismiss();

                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                dialog.dismiss();
                                }
                            })
                            .show();*/
                }
                if(categories.getSubtype().equalsIgnoreCase("roomservice"))
                {
                 /*  View v= LayoutInflater.from(view.getRootView().getContext()).inflate(R.layout.dialog_custom_box,null);
                    final AlertDialog.Builder alert = new AlertDialog.Builder(view.getRootView().getContext());
                    alert.setView(v);
                    AlertDialog dialog = alert.create();
                    dialog.show();*/

                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return categoriesList.size();
    }
}
