package com.example.sunny.hsademo.Models;

/**
 * Created by Sunny on 15/09/2017.
 */
public class Menu {

    private Integer enabled;

    private Integer id;

    private String name;

    private String type;

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
