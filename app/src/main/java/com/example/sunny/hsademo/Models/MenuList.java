package com.example.sunny.hsademo.Models;

import java.util.List;

/**
 * Created by Sunny on 14/09/2017.
 */
public class MenuList {
   private List<Menu> menu = null;

    public List<Menu> getMenu() {
        return menu;
    }

    public void setMenu(List<Menu> menu) {
        this.menu = menu;
    }

}
