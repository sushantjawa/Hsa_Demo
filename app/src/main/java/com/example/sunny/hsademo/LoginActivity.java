package com.example.sunny.hsademo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sunny on 14/09/2017.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.Email2)
    EditText Email;
    @BindView(R.id.Password2)
    EditText Password;
    @BindView(R.id.button2)
    android.widget.Button Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        Button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view==Button)
        {
        startActivity(new Intent(getApplication(),HomePage.class));
        }
    }
}
