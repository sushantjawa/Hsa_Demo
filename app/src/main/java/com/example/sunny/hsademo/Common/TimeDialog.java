package com.example.sunny.hsademo.Common;

/**
 * Created by Sunny on 12/06/2016.
 */

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

//@SuppressLint("ValidFragment")
public class TimeDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    Button txtdate;

    public TimeDialog() {

    }

    public void TimeDialog1(View view){
        txtdate=(Button) view;
    }
    public Dialog onCreateDialog(Bundle savedInstanceState) {


// Use the current date as the default date in the dialog
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);

        // Create a new instance of DatePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, min, true);


    }

    public void onTimeSet(TimePicker view, int hour, int min) {
        //show to the selected date in the text box
     String time="";
       if(hour<10) {
           time ="0"+ hour + ":" + (min);
       if(min<10)
           time ="0"+ hour + ":0" + (min);
       }
        else if( hour>9 && min<10) {
            time =hour + ":0" + (min);

       }

        else
           time =hour + ":" + (min);
        txtdate.setText(time);

    }



}
