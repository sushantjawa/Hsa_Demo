package com.example.sunny.hsademo;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sunny.hsademo.Adapter.MenuAdapter;
import com.example.sunny.hsademo.Models.MenuList;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class HomePage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    Context context;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private RecyclerView recyclerView;
    private DatabaseReference firebaseDatabase;
    private MenuList menuList;
   // List<String> Services=new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.view_toolbar );
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Homepage");
        context = this;
        firebaseDatabase = FirebaseDatabase.getInstance().getReference("Menu");
        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_categories);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);
        setHeader();

progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please Wait");
        progressDialog.show();
       ValueEventListener valueEventListener = new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                 menuList =dataSnapshot.getValue(MenuList.class);
                System.out.println("categories"+menuList.getMenu().get(2).getName());
                MenuAdapter menuAdapter =new MenuAdapter(getApplicationContext(),menuList.getMenu());
                recyclerView.setAdapter(menuAdapter);
                //categories=categoriesList.getCategories();
               // System.out.println("categoriesString");
                    progressDialog.dismiss();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("categoriesf"+databaseError.toException());
                progressDialog.dismiss();
            }
        };
    firebaseDatabase.addValueEventListener(valueEventListener);


    }


    public void setHeader() {
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.view_navigation_header, navigationView, false);
        TextView profileUserName = (TextView) header.findViewById(R.id.user_name);
        TextView userEmail = (TextView) header.findViewById(R.id.user_email);
        navigationView.addHeaderView(header);
    }
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    }
